#!/usr/bin/make

PACKAGE_VERSION := $(shell xmllint --xpath '/package/version/text()' camera_umd/package.xml)
BUILD_DATE := $(shell date "+%Y%m%d-%H%M%S")
GIT_REVISION := $(shell git rev-parse --short HEAD)
GIT_TAG := $(shell git tag --points-at HEAD | tail -n1)
MODIFIED_FILES := $(shell git status -s | wc -l | sed 's| *||')

ifeq (v$(PACKAGE_VERSION),$(GIT_TAG))
    BUILD_REVISION :=
else
    BUILD_REVISION := -$(BUILD_DATE)-$(GIT_REVISION)
endif

ifneq (0,$(MODIFIED_FILES))
    BUILD_REVISION := $(BUILD_REVISION)-m$(MODIFIED_FILES)
endif

default: build

BUILD_DIRS := build-camera_umd build-jpeg_stream build-uvc_camera

build: $(BUILD_DIRS)

$(BUILD_DIRS): build-%:
	cd $* && \
	    rm -rf debian && \
	    bloom-generate rosdebian --os-name ubuntu --os-version trusty --ros-distro indigo && \
	    sed -i -r 's|(\([^-]*)[^)]*|\1$(BUILD_REVISION)|' debian/changelog && \
	    fakeroot debian/rules binary


